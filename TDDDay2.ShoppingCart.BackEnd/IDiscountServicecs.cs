﻿namespace TDDDay2.ShoppingCart.BackEnd
{
    public interface IDiscountService
    {
        void SetDiscount(string activityName, decimal discount);
        //打折
        decimal GetRateDicsount();
        //現折
        decimal GetPriceDiscount();
      
    }
}
